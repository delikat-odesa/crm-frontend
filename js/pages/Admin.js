import React from 'react'
import {Back, Title} from "../elements/Elements";
import {Admin_settings} from "./Admin_settings";

export class Admin extends React.Component {
    render() {
        return (
            <>
                <div className="head navbar flex-nowrap justify-content-start" id="head">
                    <Back/>
                    <Title data="Админка"/>
                </div>
                <div className="content">
                    <Admin_settings/>
                </div>
            </>
        )
    }
}